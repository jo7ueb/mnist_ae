import torch
import torch.nn as nn
import torch.nn.functional as F

__all__ = ['mlp3_relu', 'mlp3_relu_act', 'mlp3_sigmoid',
           'mlp4_relu', 'mlp4_sigmoid']

class MLP3ReLU(nn.Module):
    def __init__(self, num_classes = 10, num_hidden = 512, init_weights = True):
        super().__init__()
        self.ih = nn.Linear(28 * 28, num_hidden)
        self.ho = nn.Linear(num_hidden, num_classes)

        if init_weights:
            for m in self.modules():
                if isinstance(m, nn.Linear):
                    nn.init.normal_(m.weight, 0, 0.01)
                    nn.init.constant_(m.bias, 0)

    def forward(self, x):
        x = torch.flatten(x, 1)
        x = F.relu(self.ih(x))
        x = self.ho(x)
        return x

def mlp3_relu(weights=None, progress=True, **kwargs):
    model = MLP3ReLU(**kwargs)
    if weights is not None:
        model.load_state_dict(weights.get_state_dict(progress=progress, check_hash=True))
    return model

class MLP3ReLUAct(nn.Module):
    def __init__(self, num_classes = 10, num_hidden = 512, init_weights = True):
        super().__init__()
        self.ih = nn.Linear(28 * 28, num_hidden)
        self.ho = nn.Linear(num_hidden, num_classes)

        if init_weights:
            for m in self.modules():
                if isinstance(m, nn.Linear):
                    nn.init.normal_(m.weight, 0, 0.01)
                    nn.init.constant_(m.bias, 0)

    def forward(self, x):
        x = torch.flatten(x, 1)
        actin0 = self.ih(x)
        x = F.relu(actin0)
        x = self.ho(x)
        return x, actin0

def mlp3_relu_act(weights=None, progress=True, **kwargs):
    model = MLP3ReLUAct(**kwargs)
    if weights is not None:
        model.load_state_dict(weights.get_state_dict(progress=progress, check_hash=True))
    return model

class MLP3Sigmoid(nn.Module):
    def __init__(self, num_classes = 10, num_hidden = 512, init_weights = True):
        super().__init__()
        self.ih = nn.Linear(28 * 28, num_hidden)
        self.ho = nn.Linear(num_hidden, num_classes)

        if init_weights:
            for m in self.modules():
                if isinstance(m, nn.Linear):
                    nn.init.normal_(m.weight, 0, 0.01)
                    nn.init.constant_(m.bias, 0)

    def forward(self, x):
        x = torch.flatten(x, 1)
        x = F.sigmoid(self.ih(x))
        x = self.ho(x)
        return x

def mlp3_sigmoid(weights=None, progress=True, **kwargs):
    model = MLP3Sigmoid(**kwargs)
    if weights is not None:
        model.load_state_dict(weights.get_state_dict(progress=progress, check_hash=True))
    return model

class MLP4ReLU(nn.Module):
    def __init__(self, num_classes = 10, num_hidden = 512, init_weights = True):
        super().__init__()
        self.i = nn.Linear(28 * 28, num_hidden)
        self.h = nn.Linear(num_hidden, num_hidden)
        self.o = nn.Linear(num_hidden, num_classes)

        if init_weights:
            for m in self.modules():
                if isinstance(m, nn.Linear):
                    nn.init.normal_(m.weight, 0, 0.01)
                    nn.init.constant_(m.bias, 0)

    def forward(self, x):
        x = torch.flatten(x, 1)
        x = F.relu(self.i(x))
        x = F.relu(self.h(x))
        x = self.o(x)
        return x

def mlp4_relu(weights=None, progress=True, **kwargs):
    model = MLP4ReLU(**kwargs)
    if weights is not None:
        model.load_state_dict(weights.get_state_dict(progress=progress, check_hash=True))
    return model

class MLP4Sigmoid(nn.Module):
    def __init__(self, num_classes = 10, num_hidden = 512, init_weights = True):
        super().__init__()
        self.i = nn.Linear(28 * 28, num_hidden)
        self.h = nn.Linear(num_hidden, num_hidden)
        self.o = nn.Linear(num_hidden, num_classes)

        if init_weights:
            for m in self.modules():
                if isinstance(m, nn.Linear):
                    nn.init.normal_(m.weight, 0, 0.01)
                    nn.init.constant_(m.bias, 0)

    def forward(self, x):
        x = torch.flatten(x, 1)
        x = F.sigmoid(self.i(x))
        x = F.sigmoid(self.h(x))
        x = self.o(x)
        return x

def mlp4_sigmoid(weights=None, progress=True, **kwargs):
    model = MLP4Sigmoid(**kwargs)
    if weights is not None:
        model.load_state_dict(weights.get_state_dict(progress=progress, check_hash=True))
    return model
