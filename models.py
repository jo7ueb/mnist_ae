from mlp import *

models = {
    'mlp3_relu': mlp3_relu,
    'mlp3_relu_act': mlp3_relu_act,
    'mlp3_sigmoid': mlp3_sigmoid,
    'mlp4_relu': mlp4_relu,
    'mlp4_sigmoid': mlp4_sigmoid,
}

def setup_model(name, num_classes=10, **kwargs):
    if name in models:
        fn = models[name]
    else:
        raise RuntimeError(f"Unknown model name {name}")

    return fn(num_classes=num_classes, **kwargs)
