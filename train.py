from typing import Any

import argparse
import random
import torch
from torch import optim, nn, utils, Tensor
from torch.optim.lr_scheduler import ReduceLROnPlateau, ExponentialLR
from torch.utils.data import DataLoader
from torchvision.datasets import FakeData, MNIST
import torchvision.transforms.v2 as T
from flash.core.optimizers import LAMB
import lightning as L
from lightning.pytorch.callbacks import ModelSummary, ModelCheckpoint, LearningRateMonitor, DeviceStatsMonitor
from lightning.pytorch.callbacks import RichProgressBar
from lightning.pytorch.callbacks.early_stopping import EarlyStopping
from lightning.pytorch.profilers import AdvancedProfiler, PyTorchProfiler
from lightning.pytorch.tuner import Tuner
from torchmetrics import Accuracy
from models import setup_model

class DummyDataset(L.LightningDataModule):
    def __init__(self, config: Any):
        super().__init__()
        self.transform = T.Compose(
            [
                T.ToImage(),
                T.ToDtype(torch.float32, scale=True)
            ]
        )
        self.dataset_train = FakeData(
            size=10000,
            image_size=(1, 28, 28),
            num_classes=10,
            transform=self.transform
        )
        self.dataset_eval = FakeData(
            image_size=(1, 28, 28),
            num_classes=100,
            transform=self.transform
        )
        self.dataloader_train = DataLoader(
            self.dataset_train,
            batch_size=config.batch_size,
            shuffle=True,
            pin_memory=True,
            num_workers=config.num_workers,
            prefetch_factor=2
        )
        self.dataloader_eval = DataLoader(
            self.dataset_eval,
            batch_size=config.eval_batch_size,
            shuffle=False,
            pin_memory=True,
            num_workers=config.num_workers,
            prefetch_factor=2
        )

    def train_dataloader(self):
        return self.dataloader_train

    def val_dataloader(self):
        return self.dataloader_eval

class MNISTDataset(L.LightningDataModule):
    def __init__(self, config):
        super().__init__()
        self.transform = T.Compose(
            [
                T.ToTensor(),
            ]
        )
        self.dataset_train = MNIST(
            root='./.mnist/',
            train=True,
            download=True,
            transform=self.transform
        )
        self.dataset_eval = MNIST(
            root='./.mnist/',
            train=False,
            download=True,
            transform=self.transform
        )
        self.dataloader_train = DataLoader(
            self.dataset_train,
            batch_size=config.batch_size,
            shuffle=True,
            pin_memory=True,
            num_workers=config.num_workers,
            prefetch_factor=2
        )
        self.dataloader_eval = DataLoader(
            self.dataset_eval,
            batch_size=config.batch_size,
            shuffle=False,
            pin_memory=True,
            num_workers=config.num_workers,
            prefetch_factor=2
        )

    def train_dataloader(self):
        return self.dataloader_train

    def val_dataloader(self):
        return self.dataloader_eval

class LModelWrapper(L.LightningModule):
    def __init__(self, model, params):
        super().__init__()
        self.model = model
        self.loss = nn.CrossEntropyLoss()
        self.lr = params.lr
        self.lr_gamma = params.lr_gamma
        self.lr_scheduler = params.lr_scheduler
        self.patience = params.patience
        self.optim = params.optim
        self.example_input_array = torch.Tensor(64, 1, 28, 28)

        self.train_acc_top1 = Accuracy(task="multiclass", num_classes=10, top_k=1)
        self.val_acc_top1 = Accuracy(task="multiclass", num_classes=10, top_k=1)

    def forward(self, x):
        y =  self.model(x)
        if isinstance(y, tuple):
            return y[0]
        else:
            return y

    def training_step(self, batch, batch_idx):
        x, y = batch
        y_hat = self.forward(x)
        loss = self.loss(y_hat, y)
        acc_top1 = self.train_acc_top1(y_hat, y)
        self.log("train_loss", loss)
        self.log("train_acc_top1", acc_top1)
        return {'loss':loss, 'acc_top1':acc_top1}

    def validation_step(self, batch, batch_idx):
        x, y = batch
        y_hat = self.forward(x)
        loss = self.loss(y_hat, y)
        acc_top1 = self.val_acc_top1(y_hat, y)
        self.log("val_loss", loss)
        self.log("val_acc_top1", acc_top1)
        return {'loss':loss, 'acc_top1':acc_top1}

    def configure_optimizers(self):
        if self.optim == "lamb":
            optimizer = LAMB(self.parameters(), lr=self.lr)
        else:
            optimizer = optim.AdamW(self.parameters(), lr=self.lr)

        if self.lr_scheduler == "plateau":
            scheduler = ReduceLROnPlateau(optimizer, patience=self.patience)
            opt_config = {
                'optimizer': optimizer,
                'lr_scheduler': {
                    'scheduler': scheduler,
                    'monitor': 'val_loss'
                    }
                }
            return opt_config
        else:
            scheduler = ExponentialLR(optimizer, gamma=self.lr_gamma)

        return [optimizer], [scheduler]

def main(params):
    # setup seed
    seed = params.seed if params.seed else random.randint(0, 99999999)
    L.seed_everything(seed, workers=True)

    # define a model
    model = setup_model(params.model)
    model = LModelWrapper(model, params)

    # define a dataset
    if not params.dummy:
        dataset = MNISTDataset(params)
    else:
        dataset = DummyDataset(params)

    # checkpoint settings
    checkpoint_callback = ModelCheckpoint(
        save_top_k=1,
        monitor="val_loss",
        mode="min",
        dirpath="./",
        filename="model"
    )

    # train the model
    lr_monitor = LearningRateMonitor(logging_interval='step')
    early_stopping = EarlyStopping(monitor="val_loss", mode="min", patience=args.early_patience, check_finite=True)
    stats_monitor = DeviceStatsMonitor()
    profiler_ap = AdvancedProfiler(filename="prof")
    profiler_pt = PyTorchProfiler(emit_nvtx=True)
    profiler = profiler_pt if args.profiler == "pytorch" else profiler_ap if args.profiler == "advanced" else args.profiler
    trainer = L.Trainer(limit_train_batches=params.limit_train_batches,
                        limit_val_batches=params.limit_val_batches,
                        log_every_n_steps=2,
                        max_epochs=params.epochs,
                        deterministic="warn",
                        gradient_clip_val=args.clipping,
                        fast_dev_run=params.fast_dev_run,
                        benchmark=params.benchmark,
                        accelerator=params.accelerator,
                        precision=params.precision,
                        profiler=profiler,
                        callbacks=[ModelSummary(max_depth=-1), lr_monitor, early_stopping, stats_monitor, checkpoint_callback,
                                   RichProgressBar()])

    # Tune learning rate
    if args.tune_lr:
        tuner = Tuner(trainer)
        finder = tuner.lr_find(
            model, train_dataloaders=dataset.train_dataloader(), val_dataloaders=dataset.val_dataloader()
            )
        print(finder.results)
        new_lr = finder.suggestion()
        print('suggested lr: ', new_lr)
        model.lr = new_lr

    trainer.fit(model, dataset, ckpt_path=params.resume_from_ckpt)

    # Save model state_dict
    torch.save(model.model.state_dict(), 'model.pt')

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--seed", type=int)
    parser.add_argument("--model", default="mlp3_sigmoid")
    parser.add_argument("--lr", type=float, default=0.001)
    parser.add_argument("--tune_lr", action="store_true")
    parser.add_argument("--optim", default="adam")
    parser.add_argument("--lr_scheduler", default="exp")
    parser.add_argument("--patience", type=int, default=2)
    parser.add_argument("--lr_gamma", type=float, default=0.8)
    parser.add_argument("--clipping", type=float, default=0)
    parser.add_argument("--batch_size", type=int, default=64)
    parser.add_argument("--limit_train_batches", type=float, default=1.0)
    parser.add_argument("--limit_val_batches", type=float, default=1.0)
    parser.add_argument("--early_patience", type=int, default=5)
    parser.add_argument("--num_workers", type=int, default=2)
    parser.add_argument("--fast_dev_run", action="store_true")
    parser.add_argument("--dummy", action="store_true")
    parser.add_argument("--benchmark", action="store_true")
    parser.add_argument("--profiler", default=None)
    parser.add_argument("--accelerator", default="auto")
    parser.add_argument("--precision", default=None)
    parser.add_argument("--epochs", type=int, default=20)
    parser.add_argument("--resume_from_ckpt", default=None)

    args = parser.parse_args()
    main(args)
