import torch
import torchvision
from models import setup_model
import torchvision.transforms.v2 as T
from PIL import Image

MODEL_PATH = './model.pt'
IMG_PATH = './cat.png'
#IMG_PATH = './yasu_photo.jpg'
#IMG_PATH = './neponepo.png'
SYNSET_PATH = './synset_words.txt'

def main():
    # load model
    checkpoint = torch.load(MODEL_PATH, mmap=True)
    model = setup_model('alexnet')
    model.load_state_dict(checkpoint)

    # load image to predict
    img = Image.open(IMG_PATH).convert('RGB')
    eval_transform = T.Compose(
        [
            T.Resize((224, 224)),
            T.ToImage(),
            T.ToDtype(torch.float32, scale=True),
            T.Normalize(mean=[0.4811, 0.4575, 0.4078], std=[0.2234, 0.2294, 0.2302]),
        ]
    )
    x = eval_transform(img)

    # predict
    y_hat = model(x.unsqueeze(0))[0]
    predicted_idx = int(y_hat.argmax())

    # show label
    with open(SYNSET_PATH) as f:
        lines = f.readlines()
        lines = [l.rstrip() for l in lines]

    print(predicted_idx, lines[predicted_idx])

if __name__ == '__main__':
    main()
